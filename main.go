package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	length, err := strconv.Atoi(os.Getenv("INPUT_LENGTH"))
	if err != nil {
		fmt.Printf("invalid length: %v", os.Getenv("INPUT_LENGTH"))
		os.Exit(1)
	}
	password := strings.Repeat("password", length/8+1)
	password = password[:length]
	data := []byte("password=" + password)
	os.WriteFile(os.Getenv("OUTPUT_FILE"), data, 0666)
	data = []byte("PASSWORD=" + password)
	os.WriteFile(os.Getenv("ENV_FILE"), data, 0666)
}
